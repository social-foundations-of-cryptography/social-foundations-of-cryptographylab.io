---
layout: post
title: "PhD Position in Ethnography: Social Foundations of Cryptography"
---

We are looking for a PhD researcher to conduct ethnographic research on the security needs and practices of participants in large-scale urban protests. The studentship is funded by Royal Holloway University of London as part of our Social Foundations of Cryptography project. The link to the ad can be found [here](https://www.findaphd.com/phds/project/ethnographic-explorations-of-the-security-needs-of-participants-in-protests/?p172588).

Grounded in ethnography, this PhD project sets out to understand how information security is understood, practised, negotiated and shaped by protesters. Through extended fieldwork in a particular non-UK setting (to be decided together with the candidate), it will explore the mundane, social, temporal, spatial, cultural, political notions that underpin large-scale protests and related information security practices as well as the technologies protesters rely upon. Moreover, it will study how technologies facilitate collective action and engage with participants through on-the-ground observation and engagements, during protests and related activities.

The candidate will be supervised by ethnographer Dr Rikke Bjerg Jensen (Royal Holloway University of London). They will also work alongside cryptographers Prof. Martin Albrecht (King's College London) and Dr Benjamin Dowling (Sheffield) as well as ethnographers Dr Andrea Medrado (Exeter) and Keye Tersmette (Royal Holloway University of London) to ground cryptographic security notions in findings from ethnography. 

The significance of security technology in protests is well documented in existing scholarly work. These settings, where many activities and interactions map to some form of digital communication, therefore present distinct and rich research opportunities for ethnography. Their adversarial and highly digitalised contexts, shaped by dynamic networks, provoke a series of information security questions: How is trust established – and with whom? What security expectations are held within protest groups and how do they shape collective action? How does onboarding work? What role(s) do security technologies play within protest groups? How are concerns over infiltration of networks considered and voiced? In dynamic protest settings, responses to these questions are likely to be shaped and continuously reshaped over time, making extended and immersive ethnographic fieldwork a particularly useful research approach.

This project complements existing work in the [Ethnography Group](https://ethnography.isg.rhul.ac.uk) within the Information Security Group at Royal Holloway University of London. The Ethnography Group is made up of researchers with distinct interests in using ethnographic approaches to uncover information security needs among groups with no institutional representation. Current work by members of the Ethnography Group includes exploring how information security is experienced and practised among climate activist and protest networks in the UK, Philippines and Uganda, among domestic workers in Nigeria, in post-conflict contexts with a focus on Colombia and in women support centres in Thailand, to name a few.

Applicants should have an interest in information security but come from a qualitative social science background, with at least an undergraduate degree in a field cognate to Anthropology, Human Geography, Sociology or Science and Technology Studies. Ideally, applicants will have experience in conducting ethnographic fieldwork, engaging in participant observation and/or collecting and analysing qualitative data. The studentship is open to UK applicants.

Prospective applicants are encouraged to contact Dr Rikke Bjerg Jensen (rikke.jensen@rhul.ac.uk) to discuss the project before applying.


