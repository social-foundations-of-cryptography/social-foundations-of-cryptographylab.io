---
layout: post
title: "Postdoc Position in Ethnography: Social Foundations of Cryptography"
---

We will be looking to recruit a post-doctoral researcher to work with us on our project: Social Foundations of Cryptography. As we also say elsewhere on this site, the project is funded by the UK Engineering and Physical Sciences Research Council (EPSRC) and aims to ground cryptographic security notions in findings from ethnography, through research with and within protest and activist settings. 

*The official job ad will be out soon, but here's what we will be looking for.*

The researcher will conduct up to six months of ethnographic fieldwork with protesters and/or activists in two different non-European countries, exploring the security practices and needs of participants in protests. The fieldwork will consist of, for example, daily participant observation, drawing up field notes and maps as well as involving participants through multiple modes of engagement depending on the specific context. The research will further involve both independent and collaborative post-fieldwork analysis. 

In addition to conducting and completing ethnographic research in potentially, at times, higher-risk settings, the postdoc is expected to:

   - Be an active collaborative partner in the research project as a whole
   - Contribute to research outputs in the form of publications in respectable, top-tier scientific outlets, e.g. cryptography, information security, HCI and/or interdisciplinary social science venues
   - Initiate and co-organise activities related to the research project 
   - Co-organise, participate in and present at conferences, workshops and seminars, as well as other relevant activities
   - Collaborate with the project's academic advisory board and project partner
   - Contribute to the popular dissemination of research findings

The position is full time and expected to begin on 1 April 2024, or as soon as possible thereafter, and will last for 24 months. The researcher will be based at Royal Holloway University of London within the [Information Security Group](https://www.royalholloway.ac.uk/research-and-teaching/departments-and-schools/information-security/), but will be expected to also spend time within the Department of Informatics at King's College London. The researcher will work in close collaboration with the project team, which comprises ethnographers [Rikke Bjerg Jensen (Royal Holloway)](https://pure.royalholloway.ac.uk/en/persons/rikke-bjerg-jensen) and [Andrea Medrado (Westminster)](https://www.westminster.ac.uk/about-us/our-people/directory/medrado-andrea), as well as cryptographers [Ben Dowling (Sheffield)](https://www.sheffield.ac.uk/dcs/people/academic/benjamin-dowling) and [Martin Albrecht (King's College London)](https://www.kcl.ac.uk/people/martin-albrecht).

**Required qualifications and skills**
Applicants must have completed a PhD (or be close to completion) in anthropology, sociology, STS, media studies or a related discipline within the social sciences. Applicants must further be able to document:

   - experience in planning and carrying out ethnographic fieldwork and analysis;
   - a relevant, high-quality research profile, including experience of international publication and research dissemination; and
   - the ability to communicate fluently in English (both written and spoken) at an academic level.

It is an advantage if applicants can document prior experience of working within settings that might be considered higher-risk in safety and security terms, and/or prior experience of working with marginalised groups. 

As part of the application, applicants are asked to describe how their previous research experiences are of relevance to the position (max. 1 page). Applicants also need to submit an up-to-date CV (max. 2 pages), a brief cover letter, a brief statement on the applicant's PhD project (max. 1 page) as well as any recent publications (max. 2).

The application must be submitted in English.

Applicants are encouraged to contact Rikke Bjerg Jensen (rikke.jensen@rhul.ac.uk) and/or Andrea Medrado (a.medrado@westminster.ac.uk) for an informal discussion about the project and the position.



