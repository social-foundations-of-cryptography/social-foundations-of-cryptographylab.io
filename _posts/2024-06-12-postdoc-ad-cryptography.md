---
layout: post
title: "Postdoc Position in Cryptography: Social Foundations of Cryptography"
---

We are looking for a postdoc to work with us on the social foundations of cryptography. This is a two-year full-time position based in London at a salary of £47,978 per annum.

**We.** This postdoc position is part of the EPSRC-funded project “[Social Foundations of Cryptography](https://social-foundations-of-cryptography.gitlab.io/)”. The project team consists of:

- [Rikke Bjerg Jensen](https://rikkebjerg.gitlab.io/me/), who is an ethnographer at Royal Holloway, University of London.
- [Andrea Medrado](https://www.westminster.ac.uk/about-us/our-people/directory/medrado-andrea), who is an ethnographer at Westminster University (soon Exeter University).
- [Benjamin Dowling,](https://benjamindowling.com/) who is a cryptographer at Sheffield University (soon King’s College London).
- [Keye Tersmette](https://cmes.fas.harvard.edu/people/keye-s-tersmette), who is an ethnographer at Royal Holloway, University of London.
- [Mikaela Brough](https://mikaelabrough.github.io/), who is an ethnographer at Royal Holloway, University of London.
- [me](https://malb.io/), I am a cryptographer at King’s College London.

The postdoc would work with all of us, but would be formally supervised by me at King’s College London.

**Social Foundations of Cryptography.** To motivate what we are trying to do here, consider the following excerpt from [How to Leak a Secret](https://people.csail.mit.edu/rivest/pubs/RST01.pdf), the seminal paper by Rivest, Shamir and Tauman introducing ring signatures.

> To motivate the title for this paper, suppose that Bob (also known as “Deep Throat”) is a member of the cabinet of Lower Kryptonia, and that Bob wishes to leak a juicy fact to a journalist about the escapades of the Prime Minister, in such a way that Bob remains anonymous, yet such that the journalist is convinced that the leak was indeed from a cabinet member. \[…\]
>
> A standard group signature scheme does not solve the problem, since it requires the prior cooperation of the other group members to set up, and leaves Bob vulnerable to later identification by the group manager, who may be controlled by the Prime Minister. \[…\]
>
> The correct approach is for Bob to send the story to the journalist (through an anonymizer), signed with a ring signature scheme that names each cabinet member (including himself) as a ring member. The journalist can verify the ring signature on the message, and learn that it definitely came from a cabinet member. He can even post the ring signature in his paper or web page, to prove to his readers that the juicy story came from a reputable source. However, neither he nor his readers can determine the actual source of the leak, and thus the whistleblower has perfect protection even if the journalist is later forced by a judge to reveal his “source” (the signed document).

Here, the authors consider a whistleblower setting which then motivates the definition of ring signatures. Thus, a ring signature claims to be at least also a formalisation of the social setting in which a member of a group wishes to alert outsides to something without revealing themselves while still convincing the outsider that they have access to the information being leaked. Put differently, cryptography presumes and models social relations. As such, cryptography is also a social science. However, cryptography is unaware of itself as a social science and we cryptographers more or less speculatively make up the social settings we model in our paper’s introductions. I highly recommend Jean-François Blanchette’s “[Burdens of Proof: Cryptographic Culture and Evidence Law in the Age of Electronic Documents](https://mitpress.mit.edu/9780262017510/burdens-of-proof/)” for a deeper dive into this observation.

This begs the question if cryptography gets that part of its models right? In general, we work hard to have precise definitions and definitional work is a central activity of cryptography. Yet, the correctness of this part of the definitional work is usually simply presumed: “On these questions, the literature remains silent” as Blanchette put it.

**Ethnography.** There is an established and growing body of work on the security concerns of various groups in various contexts that we could try to rely on. However, this body of work does not provide the level of detail and gradation required for establishing, questioning, critiquing or validating cryptographic security notions.

Typically, information security scholarship, concerned with establishing what security notions are in some setting, conducts interview-based studies with members of a group under study to establish their security needs. This, however, is fraught with limitations and caveats (as readily acknowledged in these works) as these interview engagements are time-limited (interviews last, say, an hour) and participants self-select to take part.

In a nutshell, this means we’re often talking to people who “look like us”: security trainers or technology-savvy people. Finally, as people working on social aspects of information security will not tire of telling you, in information security there is often a gulf between what people say and what people do.

To get around these limitations, our project relies on ethnography, an approach that relies heavily on extended fieldwork in the setting(s) under study. Here “extended” typically means several months. This allows the researchers to observe how security is practiced and reasoned about and what the group (under study) takes for granted and thus what would not necessarily be verbalised in an interview.

In the context of our project this means that we will put what we cryptographers do “at the mercy” of the data gathered by the ethnographers in the team.

**The setting.** Just like cryptography aims to be precise about the boundaries and conditions of its definitions and theorems, ethnography avoids (over)generalising and insists on giving answers for specific settings and contexts rather than abstract notions like “security”. It deals in “The participants in a front-line confrontation of protest X in country Y at time Z were chiefly concerned with forward-secrecy in case of an arrest” rather than “forward-secrecy is important to activists”. Thus, selecting relevant settings is paramount.

When it comes to studying cryptographic security notions, we need to select a setting where such notions make sense. This means, (a) there is conflict and thus “adversaries” and (b) these adversaries are somewhat restricted to computational attacks, i.e. [they do not simply beat passwords out of people](https://xkcd.com/538/). The settings we settled on for this project are large-scale protests in various regions of the world as these tick those two boxes.

**The project.** Roughly, the research design of our project is:

1.  The ethnographers on the team spend extended periods of time “in the field” (we have four different field settings planned).
2.  The data gathered is then collectively interpreted and analysed by the entire team (once the ethnographers have done their initial analyisis). Does it suggest revisions of cryptographic notions? Can we formalise these security notions?
3.  We cryptographers then do our thing: can we design for these notions? Can we cryptanalyse the currently deployed technologies in light of these security goals?

Thus, the cryptographic work in this project can range from [definitional work to constructive work](https://petsymposium.org/popets/2020/popets-2020-0082.php) and to [cryptanalytic work](https://arstechnica.com/features/2020/08/bridgefy-the-app-promoted-for-mass-protests-is-a-privacy-disaster/). A (rather limited) example of this approach is our work [on protests in Hong Kong](https://www.usenix.org/conference/usenixsecurity21/presentation/albrecht) which motivated us to [look at Telegram](https://mtpsym.github.io/).

Feel free to reach out if you’d like to discuss if this position could be for you.

**For more details on the formalities and to apply, [go here](https://www.kcl.ac.uk/jobs/090645-research-associate-cryptography).**
