---
layout: page
title: Reading List
permalink: /reading-list
---

A somewhat eclectic list of suggested readings around the topic of social foundations of cryptography. 

# For Ethnography

> Herbert, S. (2000). For Ethnography. Progress in Human Geography, 24(4), 550–568.  
> https://www.gla.ac.uk/0t4/crcees/files/summerschool/readings/summerschool09/readings/Herbert_For_Ethnography.pdf

Put better than we have been able to (and foundational to our project), Steve Herbert points to the work of ethnographers as "unearth[ing] what the group takes for granted, and thereby reveal[ing] the knowledge and meaning structures that provide the blueprint for social action." We have therefore shamelessly appropriated this as the tagline of our project.

# For Ethnography

> Atkinson, P. (2014). For Ethnography. SAGE.  
> https://uk.sagepub.com/en-gb/eur/for-ethnography/book234711

In his "For Ethnography" book, Paul Atkinson insists that ethnography requires fieldwork; comprising field sites and the ethnographer's presence, participation and observation in such sites, over time. 

# Burdens of Proof: Cryptographic Culture and Evidence Law in the Age of Electronic Documents

> Blanchette, J.-F. (2012). Burdens of Proof: Cryptographic Culture and Evidence Law in the Age of Electronic Documents. MIT Press.  
> https://mitpress.mit.edu/9780262017510/burdens-of-proof/

This work explores and documents the ad-hoc nature of cryptographic notions of security, especially of digital signatures. These notions, instead of being based on findings from the social sciences, are justified by just-so stories haunting the introductions of cryptographic papers.

# Can Johnny Build a Protocol? Co-ordinating developer and user intentions for privacy-enhanced secure messaging protocols

> Ermoshina, K., Halpin, H., & Musiani, F. (2017). Can johnny build a protocol? Co-ordinating developer and user intentions for privacy-enhanced secure messaging protocols. In European Workshop on Usable Security  
> https://www.ndss-symposium.org/wp-content/uploads/2018/03/eurousec2017_16_Ermoshina_paper.pdf

This work, based on remote and in-situ interviews was the first (to our knowledge) that documented a disconnect between some groups in higher-risk settings and secure messaging developers' design goals. This, despite the latter group giving these settings as a motivation for their work.

# Collective Information Security in Large-Scale Urban Protests: the Case of Hong Kong

> Albrecht, M. R., Blasco, J., Jensen, R. B., & Mareková, L. (2021). Collective Information Security in Large-scale Urban Protests: the Case of Hong Kong. In M. Bailey, & R. Greenstadt, USENIX Security 2021 (pp. 3363–3380). USENIX Association.  
> https://www.usenix.org/conference/usenixsecurity21/presentation/albrecht

This is our "pilot study". While this study is severely limited methodologically (we conducted remote interviews rather than an ethnographic study) it established that in this setting collective rather than individual security goals and considerations were important and that there is a mismatch between cryptographic notions of security before/after a compromise and the security needs of the participants in this study.

# Practice-Oriented Provable Security and the Social Construction of Cryptography

> Rogaway, P. (2009). Practice-Oriented Provable Security and the Social Construction of Cryptography.  
> https://www.cs.ucdavis.edu/~rogaway/papers/cc.pdf

In this invited talk at Eurocrypt 2009, Phil Rogaway used his decades-long work in practice-oriented provable security as a backdrop to show how cryptography is socially constructed, shaped by and rooted in its disciplinary norms. In our work, we argue that cryptography *is* a social science. 

There is also [a version from 2021](https://www.cs.ucdavis.edu/~rogaway/papers/pops2.pdf) of this work that adds a discussion of realism and constructionism.
