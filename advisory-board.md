---
layout: page
title: Advisory Board
permalink: /advisory-board
---

Our advisory board is made up of five pretty amazing academics with distinct specialisms relevant to our project.

## Seny Kamara

[Seny](https://cs.brown.edu/~seny/) is an associate professor of computer science at [Brown University](https://cs.brown.edu) and a Distinguished Scientist at MongoDB where he manages the Advanced Cryptography Research Group. His research is in cryptography and is driven by real-world problems from privacy, security and surveillance. He has worked extensively on the design and cryptanalysis of encrypted search algorithms. At Brown University, he co-directs the [Encrypted Systems Lab](https://esl.cs.brown.edu/) and is affiliated with the CAPS group, the Data Science Initiative, the Center for Human Rights and Humanitarian Studies and the Policy Lab. He teaches a [Algorithms for the People](https://cs.brown.edu/~seny/2952v/) course that surveys, critiques and aspires to address the ways in which computer science and technology affect marginalised populations.


## Francesca Musiani

[Francesca](https://cis.cnrs.fr/francesca-musiani/) is an associate research professor at the French National Center for Scientific Research (CNRS) and Deputy Director of the [Center for Internet and Society of CNRS](https://cis.cnrs.fr/). Her research sits across  information and communication sciences, science and technology studies (STS) and international law, with a focus on Internet governance. Francesca's work on secure messaging among human-rights activists has been foundational to the development of our project; particularly ["Can Jonny build a protocol?"](https://www.ndss-symposium.org/wp-content/uploads/2018/03/eurousec2017_16_Ermoshina_paper.pdf) where she (with co-authors) showed a mismatch between what secure messaging designers design for and what activists need. Francesca has authored several books, most recently ["Concealing for Freedom"](https://www.matteringpress.org/books/concealing-for-freedom) with Ksenia Ermoshina.

## Kenny Paterson

[Kenny](https://inf.ethz.ch/people/person-detail.paterson.html) is a professor in the Institute of Information Security at ETH Zurich, where he leads the [Applied Cryptography Group](https://appliedcrypto.ethz.ch/). His research over the last two decades is mostly in the area of cryptography, with a strong emphasis on the analysis of deployed cryptographic systems and the development of provably secure solutions to real-world cryptographic problems. He co-founded the Real World Cryptography series of workshops to support the development of this broad area and to strengthen the links between academia and industry. He has found numerous vulnerabilities in widely used protocols such as TLS, SSH and Telegram and was recognised as a fellow of the IACR in 2017.

## Sarah Pink

[Sarah](https://research.monash.edu/en/persons/sarah-pink) is a renowned design anthropologist and director of the Emerging Technologies Research Lab at [Monash University](https://www.monash.edu/emerging-tech-research-lab/home). She is a professor both in the Faculty of Art, Design and Architecture (Department of Design) and the Faculty of Information Technology (Department of Human Centred Computing). Through innovative digital, visual and sensory research and dissemination methodologies, Sarah's research focuses on emerging technologies, automation, data, digital futures, safety and design for wellbeing. She works across disciplinary fields, bringing into conversation new ethnographic approaches and design, engineering and creative practice to engage with contemporary issues and challenges. Sarah is a [2023 Laureate Fellowship](https://www.arc.gov.au/funding-research/funding-schemes/discovery-program/australian-laureate-fellowships/2023-laureate-profile-professor-sarah-pink) recipient.

## Phil Rogaway

[Phil](https://www.cs.ucdavis.edu/~rogaway/) is a professor of computer science at the [University of California, Davis](https://www.cs.ucdavis.edu). Most of his research has been on cryptography, the mathematical treatment of secure communication. His research has focused on obtaining provably good solutions to protocol problems of utility to people’s privacy and security. He has been widely regonised for this work, including the Levchin prize (2016), PET Award (2015), IACR Fellow (2012), ACM Paris Kanellakis Award (2009), and the RSA Award in Mathematics (2003). In recent years he has grown increasingly skeptical of the claimed benefits of CS, which routinely seem dwarfed by the harms we help cause. Correspondingly, he has shifted much of his attention to social and ethical issues connected to technology, especially the climate crisis and the problem of mass surveillance.
