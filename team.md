---
layout: page
title: Team
permalink: /team
---

Our team is made up of cryptographers and ethnographers. For enquiries please contact Rikke Jensen, who heads up our project.

## Martin Albrecht

[Martin](https://malb.io) is a professor of cryptography at [King’s College London](https://www.kcl.ac.uk/research/cys). He works on secure messaging and other real-world cryptographic protocols. For example, with colleagues, he found and reported vulnerabilities in [Telegram](https://mtpsym.github.io/), [Matrix](https://arstechnica.com/information-technology/2022/09/matrix-patches-vulnerabilities-that-completely-subvert-e2ee-guarantees/), [Bridgefy](https://arstechnica.com/features/2020/08/bridgefy-the-app-promoted-for-mass-protests-is-a-privacy-disaster/), [Jitsi](https://jitsi.org/blog/trust-but-verify-introducing-user-verification/) and [TLS](https://arstechnica.com/science/2015/11/researchers-poke-hole-in-custom-crypto-protecting-amazon-web-services/) implementations. He also works on defences against advanced threats on the horizon such as nation-state attackers equipped with a [quantum computer](https://www.kcl.ac.uk/news/major-project-to-advance-encryption-technology-in-the-face-of-future-threats-receives-european-funding). His [Erdős–Bacon Number](https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Bacon_number) is 6.

## Rikke Bjerg Jensen

[Rikke](https://rikkebjerg.gitlab.io/me/) is a professor in the Information Security Group at [Royal Holloway University of London](https://www.royalholloway.ac.uk/research-and-teaching/departments-and-schools/information-security/). Her work is distinctly ethnographic in nature and explores information security needs, perspectives and practices among higher-risk groups often living and working at what we might call the margins of societies. She focuses on how technology, as it is shaped by social structures, relations and interactions, facilitates multiple and collective security experiences and understandings. Most recently, she has worked within protest, seafaring, refugee and migrant, and post-conflic settings. Weirdly enough, Rikke is *also* a [published cryptographer](https://eprint.iacr.org/2021/214) (link included as *proof*!).

## Ben Dowling

[Ben](https://www.sheffield.ac.uk/dcs/people/academic/benjamin-dowling) is a lecturer of cybersecurity in the [Security of Advanced Systems Group](https://www.sheffield.ac.uk/dcs/research/groups/security) at the University of Sheffield. His work focuses on the provable security of real-world cryptographic protocols and investigates how to achieve better guarantees of security. Ben was a core part of teams that analysed the specifications of fundamental internet infrastructure, such as Transport Layer Security, Certificate Transparency and the Secure Shell Protocol, and secure messaging protocols, such as [Signal](https://www.theregister.com/2016/11/08/trust_it_results_of_signals_first_formal_crypto_analysis_are_in/), and [Matrix](https://arstechnica.com/information-technology/2022/09/matrix-patches-vulnerabilities-that-completely-subvert-e2ee-guarantees/). Finally, he is interested in provably improving the security guarantees of communication technologies against strong attackers, such as those capable of [subverting messaging service infrastructure](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=9581268), or [creating quantum computers](https://dl.acm.org/doi/pdf/10.1145/3548606.3560577).

## Andrea Medrado

[Andrea](https://cdf.exeter.ac.uk/communications/profile/index.php?web_id=medrado) is a senior lecturer in Global Communications at the [University of Exeter](https://www.exeter.ac.uk/), where she is the Director of Research for Communications in the [Communications, Drama and Film Department](https://cdf.exeter.ac.uk/). Andrea is also the Vice-President of the [International Association of Media and Communication Research (IAMCR)](https://iamcr.org/), the largest international association in the field of media and communication studies. She specialises in ethnographic research with activist groups in marginalised communities in the Global South. Her focus is on how these groups exchange skills, resources and knowledge to ensure that they remain protected in their fight for human rights and social justice. Recently, she has published the book [Media Activism, Artivism, and the Fight Against Marginalisation in the Global South: South-to-South Communication (with Isabella Rega)](https://www.routledge.com/Media-Activism-Artivism-and-the-Fight-Against-Marginalisation-in-the-Global/Medrado-Rega/p/book/9781032080833). The book emphasises the urgent need to engage in South-to-South dialogues in order to challenge colonial legacies and create more sustainable connections between Global South communities and as an essential step towards facing global problems, such as State repression, inequality and climate crises. Andrea has also published widely in peer reviewed journals, e.g. on [the tensions between online visibility and vulnerability for activists](https://www.tandfonline.com/doi/full/10.1080/1369118X.2021.1954228) and edited book collections.  

## Keye Tersmette

[Keye](https://pure.royalholloway.ac.uk/en/persons/keye-tersmette) is a political anthropologist and postdoctoral researcher in the Information Security Group at [Royal Holloway University of London](https://www.royalholloway.ac.uk/research-and-teaching/departments-and-schools/information-security/), and a recent PhD graduate from the Department of Anthropology and Center for Middle Eastern Studies at Harvard University. His research focuses on borders and borderscapes, citizenship and political rights, and political legitimacy in Oman and the Middle East more broadly. He also co-convenes the [EASA network on Anthropologies of the State](https://easaonline.org/networks/anthrostate/). 

## Mikaela Brough

[Mikaela](https://pure.royalholloway.ac.uk/en/persons/mikaela-brough) is a PhD researcher in the Information Security Group at [Royal Holloway University of London](https://www.royalholloway.ac.uk/research-and-teaching/departments-and-schools/information-security/), studying as part of the [CDT in Cyber Security for the Everyday](https://www.royalholloway.ac.uk/cdt/). She previously completed a BA at McGill University in Montréal and an MSc in Social Anthropology at the University of Oxford. Her doctoral research explores how members of social movements navigate their information security in diverse cultural contexts. She has previously published work in other areas of UK-based qualitative social research.

## Samuel Light

Sam is a PHD researcher in the Information Security Group at [Royal Holloway University of London](https://www.royalholloway.ac.uk/research-and-education/departments-and-schools/information-security/). He previously completed a BA in War Studies and Philosophy at King’s College London and an MSc in Conflict Studies at the London School of Economics. During this time, his research focused on the political impact of protest movements and activist groups. He has also published qualitative research on the strategy and institutional identity of British environmentalist movements.

## Simone Colombo

[Simone](https://simonecolombo.info) is a postdoctoral researcher in cryptography 
at [King’s College London](https://www.kcl.ac.uk/research/cys).
He completed his PhD at EPFL under the supervision of Bryan Ford.

# Project partner

We are excited to have partnered up with [WITNESS](https://www.witness.org/). Focusing on the use of video footage to document human rights abuses by providing training and education for human right defenders globally, WITNESS routinely monitor protests to ensure the rights of protesters are respected. WITNESS is a global organisation with regional teams based in countries such as Brazil, Malaysia, the Netherlands, Senegal, Turkey and the US, and partner networks in all other regions of the world. WITNESS is an integral part of the project as collaborators, participants and facilitators. 
